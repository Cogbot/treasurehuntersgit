﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (Camera))]
public class fogControl : MonoBehaviour {
	public float waterlevel;
	public Color ambColUnder;
	public Color ambColOver;

	// Use this for initialization
	void Start () {
		waterlevel = GameObject.Find ("Water").transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
		if (camera.transform.position.y < waterlevel) {
						RenderSettings.fog = true;
						RenderSettings.ambientLight = ambColUnder;
				} else {
						RenderSettings.fog = false;
						RenderSettings.ambientLight = ambColOver;
		}
	}
}
