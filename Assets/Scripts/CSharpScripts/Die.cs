﻿using UnityEngine;
using System.Collections;

public class Die : HasTimedEffect {
	public GameObject particleFinish;
	public bool dead = false;

	public override void timedBehaviour ()
	{
		if (!dead)
		{
			GameObject instance = (GameObject) Instantiate(particleFinish, transform.position, Quaternion.identity);
			Destroy (instance, 5);
		}
		Destroy (gameObject);
	}
}
