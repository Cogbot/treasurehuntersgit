﻿using UnityEngine;
using System.Collections;

public class ControlPlayer : Player {
	public Vehicle v;
	
	void FixedUpdate() {
		v.setH(Input.GetAxis ("Horizontal"));
		v.setV(Input.GetAxis("Vertical"));
	}
	
	void OnGUI()
	{
		string text = "Red: " + red + "\nGreen: " + green + "\nBlue: " + blue + "\nYellow: " + yellow;
		GUIStyle s = new GUIStyle ();
		s.fontSize = 40;
		GUI.Label (new Rect (10f, 10f, 100f, 70f), text, s);
  }
}
