﻿using UnityEngine;
using System.Collections;

public class GemData : HasTimedEffect {
	public enum GemCol{red, green, blue, yellow};
	public GemCol col;
	public bool alreadyCollected = false;

	public void DieInTime(float life) {
		GameObject.Find ("GameScripts").GetComponent<TimedEffect> ().register (GetComponent<Die> (), life);
	}
	
	public override void timedBehaviour ()
	{
		if (!alreadyCollected)
				GetComponent<SphereCollider> ().enabled = true;
	}
}