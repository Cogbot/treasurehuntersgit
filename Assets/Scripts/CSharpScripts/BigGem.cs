﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class BigGem : MonoBehaviour {

	public TerrainTools tt;
	public GameObject gemPrimative;
	public bool fired = false;

	public GameObject redEnd;
	public GameObject greenEnd;
	public GameObject blueEnd;
	public GameObject yellowEnd;

	public GameObject redVis;
	public GameObject greenVis;
	public GameObject blueVis;
	public GameObject yellowVis;

	public GameObject missedR;
	public GameObject missedG;
	public GameObject missedB;
	public GameObject missedY;

	public GemData.GemCol col;

	//cached
	private AnimationCurve xCurve, yCurve, zCurve;
	private float timeBetweenKeys;
	private float totalCurveTime;
	private Vector3 lastPosition = Vector3.zero;
	private float gemTime;
	private int keyCount;
	private List<Vector3> spawnVectors;
	private float xVec, yVec, zVec, startAngle, lastYVec;
	private GameObject particleEnd;
	private float waterHeight;

	private float startHeight;
	private float tempX, tempY, tempZ;

	void Awake() {
		transform.parent = GameObject.Find ("SpawnPoints").transform;
		Destroy(transform.GetChild(0).gameObject);
		tt = GameObject.Find ("Terrain").GetComponent<TerrainTools> ();
		System.Array enumArray = System.Enum.GetValues(typeof(GemData.GemCol));
		Random.seed = (int)System.DateTime.Now.Ticks;
		col = (GemData.GemCol)enumArray.GetValue(Random.Range (0, enumArray.Length));

		GameObject visibleBigGem;
		switch (col)
		{
		case GemData.GemCol.red: visibleBigGem = redVis; break;
		case GemData.GemCol.green: visibleBigGem = greenVis; break;
		case GemData.GemCol.blue: visibleBigGem = blueVis; break;
		default: visibleBigGem = yellowVis; break;
		}
		GameObject viewParticles = (GameObject)Instantiate (visibleBigGem, transform.position, Quaternion.identity);
		viewParticles.transform.localScale = new Vector3(2.7f,2.7f,2.7f);
		viewParticles.transform.GetChild (0).name = "hello";
		viewParticles.transform.GetChild (0).GetChild (0).GetComponent<ParticleSystem> ().startSize = 80;
		viewParticles.transform.parent = transform;

		// Find the height of the water
		waterHeight = GameObject.Find ("Water").transform.position.y;
	}

	public void OnTriggerEnter(Collider other)
	{
		Random.seed = (int)System.DateTime.Now.Ticks;
		if (fired || !other.gameObject.tag.Equals("Player")) return;

		fired = true;

		int n = (int)other.transform.parent.rigidbody.velocity.magnitude + 1;
		
		// Iterate through runs
		for (int runs=0; runs<1; runs++)
		{
			// Create an animation curves for x and y
			xCurve = new AnimationCurve ();
			yCurve = new AnimationCurve ();
			zCurve = new AnimationCurve ();
			
			totalCurveTime = 0;
			keyCount = 0;
			spawnVectors = new List<Vector3>();
			//startAngle = Random.Range(-100, 100)/200f;
			startAngle = Mathf.Atan2(other.transform.parent.rigidbody.velocity.x,other.transform.parent.rigidbody.velocity.z);
			
			for (int i=0; i<6; i++)
			{
				xVec = Mathf.Sin(startAngle+Random.Range(-6,6)/100f)*i*20 + transform.position.x;
				zVec = Mathf.Cos(startAngle+Random.Range(-6,6)/100f)*i*20 + transform.position.z;
				yVec = tt.height(xVec, zVec);
				if (i==0) {
					startHeight = yVec;
				}
				else
				{
					lastYVec = yVec;
				}
				spawnVectors.Add(new Vector3(xVec, yVec, zVec));
			}
			
			foreach (Vector3 spawnVector in spawnVectors)
			{
				// Check this isn't the first key
				if (!(keyCount == 0))
				{
					totalCurveTime += (lastPosition - spawnVector).magnitude;
				}
				xCurve.AddKey (new Keyframe (totalCurveTime, spawnVector.x));
				yCurve.AddKey (new Keyframe (totalCurveTime, spawnVector.y));
				zCurve.AddKey (new Keyframe (totalCurveTime, spawnVector.z));
				lastPosition = spawnVector;
				
				if (!(keyCount == 0))
				{
					xCurve.SmoothTangents (keyCount - 1, 0);
					yCurve.SmoothTangents (keyCount - 1, 0);
					zCurve.SmoothTangents (keyCount - 1, 0);
				}
				keyCount++;
				
				// Get rid of the box
				//					Destroy (spawn.transform.FindChild ("Cube").gameObject);
			}

			int numberOfGems = 5+n*n/20;
			lastPosition = transform.position;
			for (int gemNo=0; gemNo<numberOfGems; gemNo++)
			{
				gemTime = gemNo * totalCurveTime / numberOfGems;
				
				// Create the base gem
				tempX = xCurve.Evaluate (gemTime);
				tempZ = zCurve.Evaluate (gemTime);
				tempY = tt.height(tempX,tempZ);
				if (tt.steepness(tempX,tempZ)>0.9 && tempY>waterHeight)
				{
					lastPosition = new Vector3 (tempX,tempY,tempZ);

					GameObject baseGem = (GameObject)Instantiate (gemPrimative, lastPosition, Quaternion.identity);
	//				baseGem.transform.parent = gameObject.transform;
					baseGem.GetComponent<GemData>().DieInTime(60/n+5);
					baseGem.GetComponent<GemData> ().col = col;
					GameObject.Find ("GameScripts").GetComponent<TimedEffect> ().register (baseGem.GetComponent<GemData>(), (float)gemNo/n/2);
					switch (col)
					{
					case GemData.GemCol.red:
						baseGem.GetComponent<Die>().particleFinish = missedR; break;
					case GemData.GemCol.green:
						baseGem.GetComponent<Die>().particleFinish = missedG; break;
					case GemData.GemCol.blue:
						baseGem.GetComponent<Die>().particleFinish = missedB; break;
					default:
						baseGem.GetComponent<Die>().particleFinish = missedY; break;
					}
				}
			}
			// Old behaviour
			// Iterate through gems
			/**
				foreach (Transform spawn in run.transform) {
					// Create the base gem
					GameObject baseGem = (GameObject) Instantiate(gemPrimative, spawn.position, Quaternion.identity);
					baseGem.transform.parent = gameObject.transform;
					baseGem.GetComponent<GemData>().col = col;

					// Get rid of the box
					Destroy(spawn.transform.FindChild("Cube").gameObject);
				}
				**/
		}

		switch (col)
		{
		case GemData.GemCol.red: particleEnd = redEnd; break;
		case GemData.GemCol.green: particleEnd = greenEnd; break;
		case GemData.GemCol.blue: particleEnd = blueEnd; break;
		case GemData.GemCol.yellow: particleEnd = yellowEnd; break;
		}
		GameObject endParticles = (GameObject)Instantiate (particleEnd, transform.position, Quaternion.identity);
		Destroy (endParticles, 5);
		Destroy (gameObject);
	}
}
