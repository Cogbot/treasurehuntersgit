﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (FollowCam))]
public class VehicleSwitcher : MonoBehaviour {
	public GameObject car;
	public GameObject walk;
	public GameObject bike;
	public ClickControls clickControls;
	public GameObject spawn;
	public FollowCam followCam;
	public GameObject playerGameOb;
	public ControlPlayer player;
	public TerrainTools tt;
	public NetworkManager nm;

	void Awake() {
		Debug.Log ("Awoken");
	}

	// Use this for initialization
	public void begin () {
		spawnGM (bike);
		followCam.distance = 3;
		followCam.height = 1.5f;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.P) || Input.GetKey (KeyCode.O)) {
			/**
			Random.seed = (int)System.DateTime.Now.Ticks;
			spawn = GameObject.Find("SpawnPoints").transform.GetChild(
				Random.Range(0,GameObject.Find ("SpawnPoints").transform.childCount)).gameObject;
			GameObject instance = (GameObject) Instantiate(walk, spawn.transform.position - Vector3.forward*10, Quaternion.identity);
			player.transform.parent = instance.transform;
			player.transform.localPosition = Vector3.zero;
			followCam.target = instance.transform;
			clickControls.car = instance.transform;
			**/
			respawn();
		}
	}

	public void respawn() {
		Random.seed = (int)System.DateTime.Now.Ticks;
    	if (Random.Range(0,2)==0) {
			spawnGM(walk);
			followCam.distance = 2.5f;
			followCam.height = 2f;
	    } else {
			spawnGM(car);
			followCam.distance = 5;
			followCam.height = 2.5f;
    	}
	}
	private GameObject spawnGM(GameObject gm) {
		Random.seed = (int)System.DateTime.Now.Ticks;
		int childcount = GameObject.Find ("SpawnPoints").transform.childCount;
		Vector3 spawnpos = Vector3.one*150;
		if (childcount>0) {
			spawn = GameObject.Find("SpawnPoints").transform.GetChild(
				Random.Range(0,childcount-1)).gameObject;
			spawnpos = spawn.transform.position - Vector3.forward*10;
			spawnpos = new Vector3 (spawnpos.x,tt.height(spawnpos.x,spawnpos.z)+5,spawnpos.z);
		}
		return changeToGM (gm, spawnpos);
	}

	private GameObject changeToGM(GameObject gm, Vector3 pos) {
		GameObject instance;
		if (nm.connected) {
			instance = (GameObject) Network.Instantiate(gm, pos, Quaternion.identity, 0);
		} else {
			instance = (GameObject) Instantiate(gm, pos, Quaternion.identity);
		}
		player.v = instance.transform.GetComponent<Vehicle> ();
		player.transform.parent = instance.transform;
		player.transform.localPosition = Vector3.zero;
		followCam.target = instance.transform;
		clickControls.car = instance.transform;
		instance.transform.localRotation = 	Quaternion.Euler(Vector3.zero);
		Destroy(instance.transform.FindChild("Player").gameObject);
		return instance;
	}
}
