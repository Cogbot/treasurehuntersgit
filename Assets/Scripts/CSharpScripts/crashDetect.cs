﻿using UnityEngine;
using System.Collections;

public class crashDetect : MonoBehaviour {
	Rigidbody rb;
	
	public CarController controller;
	public float crashThreshold = 4;

	private float  lastVel;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();
		lastVel = 0;
	}
	
	// Update is called once per frame
	void Update () {
		float velChange = rb.velocity.magnitude - lastVel;

		if (velChange < -crashThreshold) {
//			Debug.Log (velChange);
			controller.hp -= Mathf.Pow(velChange, 4)/1000;
		}

		lastVel = rb.velocity.magnitude;
	}
}
