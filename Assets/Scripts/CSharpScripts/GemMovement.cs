﻿using UnityEngine;
using System.Collections;

public class GemMovement : MonoBehaviour {
	public GameObject gems;
	public GemController gemController;
	public float moveOffset;
	public static float moveOffsetMult = 5;
	public float fall = 5;

	//cached
	private float time;

	// Use this for initialization
	void Awake () {
		gems = GameObject.Find ("Gems");
		gemController = gems.transform.GetComponent <GemController>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		fall -= Time.deltaTime*40;
		if (fall < 0)
						fall = 0;
		time = gemController.timer + moveOffset * moveOffsetMult;
		transform.localPosition = new Vector3(0,Mathf.Sin (time*4)/10 + 0.5f + fall,0);
		transform.localRotation = Quaternion.Euler(315, time*100, 315);
	}
}
