﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (Rigidbody))]
public class TurnBoost: MonoBehaviour {
	public float amount = 10000;
	public Vehicle v;
	
	void LateUpdate ()
	{
		rigidbody.AddTorque (new Vector3(0, v.getH()*amount, 0));
	}
}