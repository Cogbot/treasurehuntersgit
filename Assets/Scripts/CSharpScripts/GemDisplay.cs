﻿using UnityEngine;
using System.Collections;

public class GemDisplay : MonoBehaviour {
	public GameObject visibleGemR;
	public GameObject visibleGemG;
	public GameObject visibleGemB;
	public GameObject visibleGemY;

	//cached
	private GameObject visibleGem;
	
	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag.Equals("Gem")) {
			switch(other.GetComponent<GemData>().col) {
			case GemData.GemCol.red: visibleGem = visibleGemR;
				break;
			case GemData.GemCol.green: visibleGem = visibleGemG;
				break;
			case GemData.GemCol.blue: visibleGem = visibleGemB;
				break;
			default: visibleGem = visibleGemY;
				break;
			}
			
			// Create the visible gem
			if (!other.GetComponent<GemData>().alreadyCollected) {
				GameObject visible = (GameObject) Instantiate(visibleGem, other.transform.position, Quaternion.identity);
				visible.transform.parent = other.gameObject.transform;
				visible.transform.FindChild("Cube").GetComponent<GemMovement>().moveOffset = GemController.i++;
			}
		}
	}
	
	void OnTriggerExit(Collider other) {
		if (other.gameObject.tag.Equals("Gem")) {
			// Destroy the visible gem
			Destroy(other.gameObject.transform.GetChild(0).gameObject);
		}
	}
}