﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (NetworkView))]
public class Vehicle : MonoBehaviour {
	public float h, v;
	public NetworkManager nm;

	protected void Awake() {
		nm = GameObject.Find ("GameScripts").GetComponent<NetworkManager> ();
//		Debug.Log(nm.connected);
	}

	public void setH(float newH) {
		if (nm.connected) {
			networkView.RPC("rpcSetH", RPCMode.All, newH);
		} else {
			h = newH;
		}
	}

	public float getH() { return h; }
	[RPC]
	public void rpcSetH(float newH) { h = newH;}
	
	public void setV(float newV) {
		if (nm.connected) {
			networkView.RPC("rpcSetV", RPCMode.All, newV);
		} else {
			v = newV;
    	}
  	}
	public float getV() { return v; }
	[RPC]
	public void rpcSetV(float newV) { v = newV;}
}
