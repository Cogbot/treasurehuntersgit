﻿using UnityEngine;
using System.Collections;

public class FollowCam : MonoBehaviour {
	/*
    This camera smoothes out rotation around the y-axis and height.
    Horizontal Distance to the target is always fixed.
     
    There are many different ways to smooth the rotation but doing it this way gives you a lot of control over how the camera behaves.
     
    For every of those smoothed values we calculate the wanted value and the current value.
    Then we smooth it using the Lerp function.
    Then we apply the smoothed values to the transform's position.
    */
	
	// The target we are following
	public Transform target;
	// The distance in the x-z plane to the target
	public float distance = 10.0f;
	// the height we want the camera to be above the target
	public float height = 5.0f;
	public float tiltUp = 15;
	// How much we
	public float heightDamping = 2.0f;
	public float rotationDamping = 3.0f;
	public float speedMultiplier = 40;

	// cached
	public float adjustedHeight;
	public float adjustedDist;
	public float speed;

	// Place the script in the Camera-Control group in the component menu
//	@script AddComponentMenu("Camera-Control/Smooth Follow")
		
		
	public void FixedUpdate () {
		// Early out if we don't have a target
		if (!target)
			return;

		// Adjust height and distance based on speed
		speed = Mathf.Lerp(speed,(target.rigidbody.velocity.magnitude + speedMultiplier) / speedMultiplier,.5f);
		adjustedDist = distance * speed;
		adjustedHeight = height * speed;

		// Calculate the current rotation angles
		float wantedRotationAngle = target.eulerAngles.y;
		float wantedHeight = target.position.y + adjustedHeight;
		
		float currentRotationAngle = transform.eulerAngles.y;
		float currentHeight = transform.position.y;
		
		// Damp the rotation around the y-axis
		currentRotationAngle = Mathf.LerpAngle (currentRotationAngle, wantedRotationAngle, rotationDamping*0.2f);
		
		// Damp the height
		currentHeight = Mathf.Lerp (currentHeight, wantedHeight, heightDamping*0.2f);
		
		// Convert the angle into a rotation
		Quaternion currentRotation = Quaternion.Euler (0, currentRotationAngle, 0);
		
		// Set the position of the camera on the x-z plane to:
		// distance meters behind the target
		transform.position = target.position;
		transform.position -= currentRotation * Vector3.forward * adjustedDist;
		
		// Set the height of the camera
		Vector3 pos = transform.position;
		pos.y = currentHeight;
		transform.position = pos;

		// move upwards if intersect with ground
		RaycastHit hit;
		if (Physics.Raycast (transform.position + Vector3.up * 100, -Vector3.up, out hit, 300)) {
			if (hit.distance<100+adjustedHeight) transform.position = hit.point + Vector3.up*adjustedHeight;
		}

		// Always look at the target
		transform.LookAt (target);
		
		// Tilt camera up a bit
		Vector3 getRot = transform.rotation.eulerAngles;
		getRot.x -= tiltUp;
		transform.rotation = Quaternion.Euler(getRot);

	}
}
