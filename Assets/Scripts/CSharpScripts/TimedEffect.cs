﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public abstract class HasTimedEffect : MonoBehaviour {
	public abstract void timedBehaviour();
}

/**
 * Register with this script to have an effect happen in a set amount of time
 **/
public class TimedEffect : MonoBehaviour {
	public float time = 0;

	private SortedList<float, HasTimedEffect> events = new SortedList<float, HasTimedEffect> ();

	//cached
	private List<float> toDelete = new List<float>();

	public void register(HasTimedEffect applicant, float delay)
	{
		lock(events)
		{
			if (!events.Keys.Contains(time+delay))
					events.Add (time+delay, applicant);
			time += 0.001f;
		}
	}

	// Update is called once per frame
	void Update () {
		time += Time.deltaTime;
		if (events.Keys.Count == 0)
						return;

		lock(events)
		{
			if (toDelete.Count>0) toDelete = new List<float>();
			foreach (float f in events.Keys)
			{
				if (f>time) break;
				events[f].timedBehaviour();
				toDelete.Add(f);
			}

			foreach (float f in toDelete)
			{
				events.Remove(f);
			}
		}
	}
}
