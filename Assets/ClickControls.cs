using UnityEngine;
using System.Collections;

public class ClickControls : MonoBehaviour {
	public Transform car;
	public LayerMask layers;

	void Update () {
		CarController carScript = null;
		if (car != null) carScript = car.gameObject.GetComponent ("CarController") as CarController;

		if (carScript != null) {
						carScript = car.gameObject.GetComponent ("CarController") as CarController;
						carScript.touchForwards = 0;
						carScript.touchBackwards = 0;
						carScript.touchSteerInput = 0;
						if (Input.GetMouseButton (0)) {
								// Construct a ray from the current mouse coordinates
								Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
								RaycastHit hit;
								if (Physics.Raycast (ray, out hit, 1000, layers)) {

										float x = car.position.x - hit.point.x;
										float z = car.position.z - hit.point.z;
										float angle = Mathf.Atan2 (x, z);

										float angle2 = car.eulerAngles.y * Mathf.Deg2Rad - Mathf.PI;

										float wholeAngle = angle - angle2;
										if (wholeAngle < Mathf.PI)
												wholeAngle += Mathf.PI * 2;
										if (wholeAngle > Mathf.PI)
												wholeAngle -= Mathf.PI * 2;

//				float dist = x*x + y*y;

										if (wholeAngle < 1.7 && wholeAngle > -1.7) {
												carScript.touchForwards = 1;
										} else {
												carScript.touchBackwards = 1;
										}

										carScript.touchSteerInput = Mathf.Clamp (wholeAngle, -1, 1) * 2;
								}
						}
				}
		}
}
