using UnityEngine;
using System.Collections;

[RequireComponent(typeof (ParticleSystem))]
public class ParticleCol : MonoBehaviour {
	public ParticleSystem part;

	// Use this for initialization
	void Start () {
		DetectTerrainSplat splat = (DetectTerrainSplat) GameObject.Find ("Terrain").GetComponent("DetectTerrainSplat");
		splat.subscribe (gameObject);
	}
	
	// Update is called once per frame
	void Update () {

	}
}
