using UnityEngine;
using System.Collections;

public class ObjectRotator : MonoBehaviour 
{
	#region Public Fields
	public float rotationSpeed = 10.0f;
	#endregion
	
	
	// Update is called once per frame
	void Update () 
	{
		transform.Rotate(0,rotationSpeed*Time.deltaTime, 0);
	}
}
