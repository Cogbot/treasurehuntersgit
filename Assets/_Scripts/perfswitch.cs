using UnityEngine;
using System.Collections;

public class perfswitch : MonoBehaviour {

public GameObject cam;
private bool Post = true;
	
	// Update is called once per frame
	void OnGUI () {
		Post = GUI.Toggle(new Rect(Screen.width - 200, 20, 200, 30), Post, "Toggle Post-Process (Faster)");
		GetComponent<Vignetting>().enabled = Post;
		GetComponent<DepthOfField34>().enabled = Post;
		GetComponent<BloomAndLensFlares>().enabled = Post;
		GetComponent<AntialiasingAsPostEffect>().enabled = Post;

			
	}
}
