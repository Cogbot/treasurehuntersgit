using UnityEngine;
using System.Collections;

public class PingPong_Substance : MonoBehaviour 
{
	public string paintParm = "Paint_Removal";
	public string bumpParm = "";
	public float bumpValue = 3.0f;
	public float cycleTime = 10.0f;
	
	void Start()
	{
		
	}
	
	
	// Update is called once per frame
	void Update () 
	{
		ProceduralMaterial mySub = renderer.sharedMaterial as ProceduralMaterial;
		if(mySub)
		{
			float lerp = Mathf.PingPong(Time.time * 2 / cycleTime, 1);
			mySub.SetProceduralFloat(paintParm, lerp);
			mySub.SetProceduralFloat(bumpParm, bumpValue);
			mySub.RebuildTextures();
			
		}
	}
}
