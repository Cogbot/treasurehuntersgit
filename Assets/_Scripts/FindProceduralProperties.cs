using UnityEngine;
using System;
using System.Collections;

public class FindProceduralProperties : MonoBehaviour 
{
	#region Public Fields
	public Vector2 scroller;
	public Rect mainGuiRect;
	public Texture2D colorPicker;
	public Vector2 colorPickerSize;
	public Transform engineGEO;
	public Transform cockpitGEO;
	public GUISkin curGUISkin;
	public Texture2D logo;
	
	public int toolBarInt = 0;
	public string[] toolBarStrings = {"Engine", "Cockpit"};
	public Rect toolBarRect;
	public Rect logoRect;
	#endregion
	
	#region Private Fields
	private ProceduralPropertyDescription[] curProperties;
	private ProceduralPropertyDescription[] curCockpitProperties;
	private ProceduralMaterial engineSubstance;
	private ProceduralMaterial cockpitSubstance;
	#endregion
	
	void Start()
	{
		toolBarRect = new Rect(0,0,Screen.width * 0.25f, toolBarRect.height);
		mainGuiRect = new Rect(mainGuiRect.x, mainGuiRect.y + toolBarRect.height,
		                       Screen.width * 0.25f, Screen.height - toolBarRect.height);
		logoRect = new Rect(Screen.width - logo.width, Screen.height - logo.height, logo.width, logo.height);
		
		engineSubstance = engineGEO.renderer.sharedMaterial as ProceduralMaterial;
		cockpitSubstance = cockpitGEO.renderer.sharedMaterial as ProceduralMaterial;
		curProperties = engineSubstance.GetProceduralPropertyDescriptions();
		curCockpitProperties = engineSubstance.GetProceduralPropertyDescriptions();
		
		/*
		int i = 0;
		while(i < curProperties.Length)
		{
			Debug.Log(curProperties[i].name.ToString() + " is of type: " + curProperties[i].type.ToString());
			i++;
		}
		*/
		
	}
	
	// Use this for initialization
	void OnGUI() 
	{	
		GUI.skin = curGUISkin;
		//create ToolBar
		toolBarInt = GUI.Toolbar(toolBarRect,toolBarInt, toolBarStrings, "TabButton");
		
		//place Logo
		GUI.Label(logoRect, logo);
		
		//Create the GUI Windows
		if(engineSubstance)
		{
			if(toolBarInt == 0)
			{
				GUI.Window(0, mainGuiRect, VehiclePropertiesGUI, "");
			}
			else
			{
				GUI.Window(1, mainGuiRect, CockpitPropertiesGUI, "");
			}
		}
		else
		{
			Debug.LogWarning("Cant find a Substance on this material.  " +
							"Assign a Transform with a substance materials attached!");
		}
		
	}
	
	void VehiclePropertiesGUI(int guiID)
	{
		GUI.skin = curGUISkin;
		//Start the type of GUILayout you need 
		scroller = GUILayout.BeginScrollView(scroller);
		
		//put the meat of your GUI elements in here
		int i = 0;
		while(i < curProperties.Length)
		{
			//get the current property and the type and store them in a new set of variables
			ProceduralPropertyDescription curProperty = curProperties[i];
			ProceduralPropertyType curType = curProperties[i].type;
			
			//test the tpye and create the correct GUI element for it
			if(curType == ProceduralPropertyType.Float)
			{
				if(curProperty.hasRange)
				{
					GUILayout.Label(curProperty.name);
					float curFloat = engineSubstance.GetProceduralFloat(curProperty.name);
					float oldFloat = curFloat;
					curFloat = GUILayout.HorizontalSlider(curFloat, curProperty.minimum, curProperty.maximum);
					if(curFloat != oldFloat)
					{
						engineSubstance.SetProceduralFloat(curProperty.name, curFloat);
					}
				}
			}
			else if(curType == ProceduralPropertyType.Color4)
			{

				GUILayout.Label(curProperty.name);
				Color curColor = engineSubstance.GetProceduralColor(curProperty.name);
				Color oldColor = curColor;
				
				//get teh current Rectangle for this picker
				Rect curRect = GUILayoutUtility.GetLastRect();
					
				//create the color Picker
				if(GUILayout.RepeatButton(colorPicker))
				{	
					//get the latest mouse position
					Vector2 pickPos = Event.current.mousePosition;
					float currentPickerPosX = pickPos.x - curRect.x - 10.0f;
					float currentPickerPosY = pickPos.y - curRect.y + 5.0f; 
					
					
					
					int aaa = Convert.ToInt32(currentPickerPosX);
					int bbb = Convert.ToInt32(currentPickerPosY);
					
					//Debug.Log("Rect Vals are: " + curRect.x.ToString() + " " + curRect.y +
					//          " "  + "Current Pixel Vals are: " + aaa.ToString() + " " + bbb.ToString());
					
					
					Color col = colorPicker.GetPixel(aaa, -bbb);
					curColor = col;
					
				}
				//curRect = GUILayoutUtility.GetLastRect();
			
				//Change the procedural value if the cur color has changed
				if(curColor != oldColor)
				{
					engineSubstance.SetProceduralColor(curProperty.name, curColor);
				}
				
			}
			else if(curType == ProceduralPropertyType.Enum)
			{
				GUILayout.Label(curProperty.name);
				int enumVal = engineSubstance.GetProceduralEnum(curProperty.name);
				int oldEnumVal = enumVal;
				string[] enumOptions = curProperty.enumOptions;
				enumVal = GUILayout.SelectionGrid(enumVal, enumOptions, 1);
				if(enumVal != oldEnumVal)
				{
					engineSubstance.SetProceduralEnum(curProperty.name, enumVal);
				}
				
			}

			
			i++;
		}
		
		//rebuild the substance texture
		engineSubstance.RebuildTextures();
		
		//end the type of GUILayout you have
		GUILayout.EndScrollView();
	}
	
	void CockpitPropertiesGUI(int guiID)
	{
		GUI.skin = curGUISkin;
		//Start the type of GUILayout you need 
		scroller = GUILayout.BeginScrollView(scroller);
		
		//put the meat of your GUI elements in here
		int i = 0;
		while(i < curCockpitProperties.Length)
		{
			//get the current property and the type and store them in a new set of variables
			ProceduralPropertyDescription curProperty = curCockpitProperties[i];
			ProceduralPropertyType curType = curCockpitProperties[i].type;
			
			//test the tpye and create the correct GUI element for it
			if(curType == ProceduralPropertyType.Float)
			{
				if(curProperty.hasRange)
				{
					GUILayout.Label(curProperty.name);
					float curFloat = cockpitSubstance.GetProceduralFloat(curProperty.name);
					float oldFloat = curFloat;
					curFloat = GUILayout.HorizontalSlider(curFloat, curProperty.minimum, curProperty.maximum);
					if(curFloat != oldFloat)
					{
						cockpitSubstance.SetProceduralFloat(curProperty.name, curFloat);
					}
				}
			}
			else if(curType == ProceduralPropertyType.Color4)
			{

				GUILayout.Label(curProperty.name);
				Color curColor = engineSubstance.GetProceduralColor(curProperty.name);
				Color oldColor = curColor;
				
				//get teh current Rectangle for this picker
				Rect curRect = GUILayoutUtility.GetLastRect();
					
				//create the color Picker
				if(GUILayout.RepeatButton(colorPicker))
				{	
					//get the latest mouse position
					Vector2 pickPos = Event.current.mousePosition;
					float currentPickerPosX = pickPos.x - curRect.x - 10.0f;
					float currentPickerPosY = pickPos.y - curRect.y + 5.0f; 
					
					
					
					int aaa = Convert.ToInt32(currentPickerPosX);
					int bbb = Convert.ToInt32(currentPickerPosY);
					
					//Debug.Log("Rect Vals are: " + curRect.x.ToString() + " " + curRect.y +
					//          " "  + "Current Pixel Vals are: " + aaa.ToString() + " " + bbb.ToString());
					
					
					Color col = colorPicker.GetPixel(aaa, -bbb);
					curColor = col;
					
				}
				//curRect = GUILayoutUtility.GetLastRect();
			
				//Change the procedural value if the cur color has changed
				if(curColor != oldColor)
				{
					cockpitSubstance.SetProceduralColor(curProperty.name, curColor);
				}
				
			}
			else if(curType == ProceduralPropertyType.Enum)
			{
				GUILayout.Label(curProperty.name);
				int enumVal = cockpitSubstance.GetProceduralEnum(curProperty.name);
				int oldEnumVal = enumVal;
				string[] enumOptions = curProperty.enumOptions;
				enumVal = GUILayout.SelectionGrid(enumVal, enumOptions, 1);
				if(enumVal != oldEnumVal)
				{
					cockpitSubstance.SetProceduralEnum(curProperty.name, enumVal);
				}
				
			}

			
			i++;
		}
		
		//rebuild the substance texture
		cockpitSubstance.RebuildTextures();
		
		//end the type of GUILayout you have
		GUILayout.EndScrollView();
	}
}
