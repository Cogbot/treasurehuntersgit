using UnityEngine;
using System;
using System.Collections;

public class SubstanceGUI : MonoBehaviour 
{
	
	#region Public Fields
	public Rect mainGuiRect;
	public Vector2 scrollVal;
	public Texture2D colorPicker;
	public Transform engineGEO;
	public Transform cockpitGEO;
	#endregion
	
	#region Private Fields
	private ProceduralPropertyDescription[] curProperties;
	private ProceduralMaterial engineSubstance;
	#endregion
	
	
	// Use this for initialization
	void Start () 
	{
		engineSubstance = engineGEO.renderer.sharedMaterial as ProceduralMaterial;
		curProperties = engineSubstance.GetProceduralPropertyDescriptions();
		
		/*
		int i = 0;
		while(i < curProperties.Length)
		{
			Debug.Log(curProperties[i].name.ToString() + " is of type: " + curProperties[i].type.ToString());
			i++;
		}
		*/
		
	}
	
	void OnGUI()
	{
		mainGuiRect = new Rect(mainGuiRect.x, mainGuiRect.y, Screen.width * 0.25f, Screen.height);
		if(engineSubstance)
		{
			GUI.Window(0, mainGuiRect, VehiclePropertiesGUI, "Engine Tweaks");  
		}
		else
		{
			Debug.LogWarning("Cant find a substance on this Transform: " + engineGEO.name.ToString());
		}
	}
	
	void VehiclePropertiesGUI(int guiID)
	{
		//start GUI Layout
		scrollVal = GUILayout.BeginScrollView(scrollVal);
		
		
		//loop through properties
		int i = 0;
		while(i < curProperties.Length)
		{
			ProceduralPropertyDescription curProperty = curProperties[i];
			ProceduralPropertyType curtype = curProperties[i].type;
			
			//create some slider for the floats
			if(curtype == ProceduralPropertyType.Float)
			{
				if(curProperty.hasRange)
				{
					GUILayout.Label(curProperty.name);
					float curFloat = engineSubstance.GetProceduralFloat(curProperty.name);
					float oldFloat = curFloat;
					curFloat = GUILayout.HorizontalSlider(curFloat, curProperty.minimum, curProperty.maximum);
					if(curFloat != oldFloat)
					{
						engineSubstance.SetProceduralFloat(curProperty.name, curFloat);
					}
				}
			}
			else if(curtype == ProceduralPropertyType.Color4)
			{
				GUILayout.Label(curProperty.name);
				Color curColor = engineSubstance.GetProceduralColor(curProperty.name);
				Color oldColor = curColor;  
				
				
				//get the last rectangle for this picker
				Rect curRect = GUILayoutUtility.GetLastRect();
				
				if(GUILayout.RepeatButton(colorPicker))
				{
					Vector2 mousePosition = Event.current.mousePosition;
					float currentPickerPosX = mousePosition.x - curRect.x - 10.0f;
					float currentPickerPosY = mousePosition.y - curRect.y + 5.0f;
					
					int x = Convert.ToInt32(currentPickerPosX);
					int y = Convert.ToInt32(currentPickerPosY);
					
					Color col = colorPicker.GetPixel(x, -y);
					curColor = col;
				}
				
				
				if(curColor != oldColor)
				{
					engineSubstance.SetProceduralColor(curProperty.name, curColor);
				}
			}
			else if(curtype == ProceduralPropertyType.Enum)
			{
				GUILayout.Label(curProperty.name);
				int enumVal = engineSubstance.GetProceduralEnum(curProperty.name);
				int oldEnumVal = enumVal;
				string[] enumOptions = curProperty.enumOptions;
				enumVal = GUILayout.SelectionGrid(enumVal, enumOptions, 1);
				if(enumVal != oldEnumVal)
				{
					engineSubstance.SetProceduralEnum(curProperty.name, enumVal);
				}
			}
			
			i++;
		}
		
		
		//rebuild the substance material
		engineSubstance.RebuildTextures();
		
		GUILayout.EndScrollView();
	}

}
