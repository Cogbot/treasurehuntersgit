using UnityEngine;
using System.Collections;

public class Decal_Selector : MonoBehaviour 
{
	public enum DecalVals {Decal1 = 0, Decal2 = 1, Decal3 = 2, Decal4 = 3};
	public DecalVals curValue;	
	public int curInt;
	
	public string enumRangeProperty = "SelectDecal";
	public ProceduralMaterial curSubstance;
	
	// Use this for initialization
	void Start () 
	{
		curInt = 0;
		
		//get the substance material attached to this object
		curSubstance = renderer.sharedMaterial as ProceduralMaterial;
	}
	
	// Update is called once per frame
	void Update () 
	{
		//increase int value with the keyboard
		if(Input.GetKeyDown(KeyCode.UpArrow))
		{
			curInt += 1;
		}
		
		if(Input.GetKeyDown(KeyCode.DownArrow))
		{
			curInt -= 1;
		}
		
		//keep the current int within the index values
		curInt = Mathf.Clamp(curInt, 0, 3);
		
		//switch based off of key board input
		switch(curInt)
		{
			case 0:
				curValue = Decal_Selector.DecalVals.Decal1;
				break;
			case 1:
				curValue = Decal_Selector.DecalVals.Decal2;
				break;
			case 2:
				curValue = Decal_Selector.DecalVals.Decal3;
				break;
			case 3:
				curValue = Decal_Selector.DecalVals.Decal4;
				break;
			default:
				curValue = Decal_Selector.DecalVals.Decal1;
				break;
		}
		
		//change the substance based off of the current enum value
		curSubstance.SetProceduralEnum(enumRangeProperty, curInt);
		curSubstance.RebuildTextures();
		
	}
}
