using UnityEngine;
using System.Collections;

public class LeftGround : MonoBehaviour {
	
	public Wing wing;
	public CarController controller;
	public float takeOffPower = 1;
	
	void OnTriggerEnter (Collider col)
	{
		if(col.gameObject.tag == "Terrain")
		{
			controller.upOffset = 1;
			controller.speed = 0;
			controller.speedBoost = 0;
			wing.liftCoefficient = 0;
		}
	}
	
	void OnTriggerExit (Collider col)
	{
		if(col.gameObject.tag == "Terrain" && takeOffPower > 0)
		{
			controller.speedBoost = 100;
			controller.upOffset = 0;
			controller.speed = 300;
//			wing.liftCoefficient = 12200 * takeOffPower;
		}
	}
}
