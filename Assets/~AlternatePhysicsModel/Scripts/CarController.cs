using UnityEngine;
using System.Collections;

// This class is repsonsible for controlling inputs to the car.
// Change this code to implement other input types, such as support for analogue input, or AI cars.
[RequireComponent (typeof (Drivetrain))]
public class CarController : Vehicle {

	public float hp = 1;

	public Wing wing;
	public int speedBoost = 0;

	Rigidbody body;
	public float stability = 0.3f;
	public float speed = 2.0f;
	private float accelKey, brakeKey, steerInput;

	public float upOffset = 0;

	// Add all wheels of the car here, so brake and steering forces can be applied to them.
	public Wheel[] wheels;
	
	// A transform object which marks the car's center of gravity.
	// Cars with a higher CoG tend to tilt more in corners.
	// The further the CoG is towards the rear of the car, the more the car tends to oversteer. 
	// If this is not set, the center of mass is calculated from the colliders.
	public Transform centerOfMass;

	// A factor applied to the car's inertia tensor. 
	// Unity calculates the inertia tensor based on the car's collider shape.
	// This factor lets you scale the tensor, in order to make the car more or less dynamic.
	// A higher inertia makes the car change direction slower, which can make it easier to respond to.
	public float inertiaFactor = 1.5f;
	
	// current input state
	public float touchForwards = 0;
	public float touchBackwards = 0;
	public float touchSteerInput = 0;
	float brake;
	float throttle;
	float throttleInput;
	float steering;
	float lastShiftTime = -1;
	float handbrake;
	float anglefactor;
		
	// cached Drivetrain reference
	Drivetrain drivetrain;
	
	// How long the car takes to shift gears
	public float shiftSpeed = 0.8f;
	

	// These values determine how fast throttle value is changed when the accelerate keys are pressed or released.
	// Getting these right is important to make the car controllable, as keyboard input does not allow analogue input.
	// There are different values for when the wheels have full traction and when there are spinning, to implement 
	// traction control schemes.
		
	// How long it takes to fully engage the throttle
	public float throttleTime = 1.0f;
	// How long it takes to fully engage the throttle 
	// when the wheels are spinning (and traction control is disabled)
	public float throttleTimeTraction = 10.0f;
	// How long it takes to fully release the throttle
	public float throttleReleaseTime = 0.5f;
	// How long it takes to fully release the throttle 
	// when the wheels are spinning.
	public float throttleReleaseTimeTraction = 0.1f;

	// Turn traction control on or off
	public bool tractionControl = true;
	
	
	// These values determine how fast steering value is changed when the steering keys are pressed or released.
	// Getting these right is important to make the car controllable, as keyboard input does not allow analogue input.
	
	// How long it takes to fully turn the steering wheel from center to full lock
	public float steerTime = 1.2f;
	// This is added to steerTime per m/s of velocity, so steering is slower when the car is moving faster.
	public float veloSteerTime = 0.1f;

	// How long it takes to fully turn the steering wheel from full lock to center
	public float steerReleaseTime = 0.6f;
	// This is added to steerReleaseTime per m/s of velocity, so steering is slower when the car is moving faster.
	public float veloSteerReleaseTime = 0f;
	// When detecting a situation where the player tries to counter steer to correct an oversteer situation,
	// steering speed will be multiplied by the difference between optimal and current steering times this 
	// factor, to make the correction easier.
	public float steerCorrectionFactor = 4.0f;

	// Used by SoundController to get average slip velo of all wheels for skid sounds.
	public float slipVelo {
		get {
			float val = 0.0f;
			foreach(Wheel w in wheels)
				val += w.slipVelo / wheels.Length;
			return val;
		}
	}

	// Initialize
	void Start () 
	{
		if (centerOfMass != null)
			rigidbody.centerOfMass = centerOfMass.localPosition;
		rigidbody.inertiaTensor *= inertiaFactor;
		drivetrain = GetComponent (typeof (Drivetrain)) as Drivetrain;

		Transform trs = transform;
		while (trs != null && trs.rigidbody == null)
			trs = trs.parent;
		if (trs != null)
			body = trs.rigidbody;
		 
	}
	
	public void Update () 
	{
		// Steering
		Vector3 carDir = transform.forward;
		float fVelo = rigidbody.velocity.magnitude;
		Vector3 veloDir = rigidbody.velocity * (1/fVelo);
		float angle = -Mathf.Asin(Mathf.Clamp( Vector3.Cross(veloDir, carDir).y, -1, 1));
		float optimalSteering = angle / (wheels[0].maxSteeringAngle * Mathf.Deg2Rad);
		if (fVelo < 1)
			optimalSteering = 0;

		steerInput = h/3;

//		float steerInput = touchSteerInput;
//		if (Input.GetKey (KeyCode.LeftArrow))
//			steerInput = -1;
//		if (Input.GetKey (KeyCode.RightArrow))
//			steerInput = 1;

//		if (touchForwards>0 || touchBackwards>0)
//						steerInput = touchSteerInput;

		// Allow to move in the air
		if (speedBoost > 0) {
			body.AddTorque(Vector3.up*steerInput*speedBoost*10);
			if (steerInput>0) {
				body.AddForceAtPosition (speedBoost * transform.right * throttleInput * 100, body.position + transform.up * upOffset);

			} else if (steerInput<0) {
				body.AddForceAtPosition (speedBoost * transform.right * throttleInput * -100, body.position + transform.up * upOffset);

			}
		}

		if (steerInput < steering)
		{
			float steerSpeed = (steering>0)?(1/(steerReleaseTime+veloSteerReleaseTime*fVelo)) :(1/(steerTime+veloSteerTime*fVelo));
			if (steering > optimalSteering)
				steerSpeed *= 1 + (steering-optimalSteering) * steerCorrectionFactor;
			steering -= steerSpeed * Time.deltaTime;
			if (steerInput > steering)
				steering = steerInput;
		}
		else if (steerInput > steering)
		{
			float steerSpeed = (steering<0)?(1/(steerReleaseTime+veloSteerReleaseTime*fVelo)) :(1/(steerTime+veloSteerTime*fVelo));
			if (steering < optimalSteering)
				steerSpeed *= 1 + (optimalSteering-steering) * steerCorrectionFactor;
			steering += steerSpeed * Time.deltaTime;
			if (steerInput < steering)
				steering = steerInput;
		}
		
		// Throttle/Brake
		if (v>0) {
			accelKey = v;
			brakeKey = 0;
		} else {
			accelKey = 0;
			brakeKey = -v;
		}
//		if (touchForwards>0)
//						accelKey = true;
//		if (touchBackwards>0)
//						brakeKey = true;

		
		if (drivetrain.automatic && drivetrain.gear == 0)
		{
			float hold = accelKey;
			accelKey = brakeKey;
			brakeKey = hold;
		}
		
		if (Input.GetKey (KeyCode.LeftShift))
		{
			throttle += Time.deltaTime / throttleTime;
			throttleInput += Time.deltaTime / throttleTime;
		}
		else if (accelKey>0)
		{
			if (drivetrain.slipRatio < 0.10f)
				throttle += Time.deltaTime / throttleTime;
			else if (!tractionControl)
				throttle += Time.deltaTime / throttleTimeTraction;
			else
				throttle -= Time.deltaTime / throttleReleaseTime;

			if (throttleInput < 0)
				throttleInput = 0;
			throttleInput += Time.deltaTime / throttleTime;
//			body.MovePosition(body.transform+body.GetPointVelocity);
			brake = 0;
		}
		else 
		{
			if (drivetrain.slipRatio < 0.2f)
				throttle -= Time.deltaTime / throttleReleaseTime;
			else
				throttle -= Time.deltaTime / throttleReleaseTimeTraction;
		}
		throttle = Mathf.Clamp01 (throttle);

		if (brakeKey>0)
		{
			if (drivetrain.slipRatio < 0.2f)
				brake += Time.deltaTime / throttleTime;
			else
				brake += Time.deltaTime / throttleTimeTraction;
			throttle = 0;
			throttleInput -= Time.deltaTime / throttleTime;
		}
		else 
		{
			if (drivetrain.slipRatio < 0.2f)
				brake -= Time.deltaTime / throttleReleaseTime;
			else
				brake -= Time.deltaTime / throttleReleaseTimeTraction;
		}
		brake = Mathf.Clamp01 (brake);
		throttleInput = Mathf.Clamp (throttleInput, -1, 1);

		// Adjust based on angle of car
		anglefactor = (Vector3.Dot (transform.up, Vector3.up)-0.5f)*2;

		body.AddForceAtPosition (speedBoost * transform.forward * throttleInput, body.position + transform.up * upOffset);
				
		// Handbrake
		handbrake = Mathf.Clamp01 ( handbrake + (Input.GetKey (KeyCode.Space)? Time.deltaTime: -Time.deltaTime) );
		
		// Gear shifting
		float shiftThrottleFactor = Mathf.Clamp01((Time.time - lastShiftTime)/shiftSpeed) * anglefactor*anglefactor;
		drivetrain.throttle = throttle * shiftThrottleFactor;
		drivetrain.throttleInput = throttleInput;
		
		if(Input.GetKeyDown(KeyCode.A))
		{
			lastShiftTime = Time.time;
			drivetrain.ShiftUp ();
		}
		if(Input.GetKeyDown(KeyCode.Z))
		{
			lastShiftTime = Time.time;
			drivetrain.ShiftDown ();
		}

		// Apply inputs
		foreach(Wheel w in wheels)
		{
			w.brake = brake;
			w.handbrake = handbrake;
			w.steering = steering;
		}
	}

	void OnDestroy() {
		DetectTerrainSplat splat = (DetectTerrainSplat) GameObject.Find ("Terrain").GetComponent("DetectTerrainSplat");
		foreach(Wheel w in wheels) {
			splat.unsubscribe(w.transform.FindChild("tyreSmoke").gameObject);
			splat.unsubscribe(w.transform.FindChild("tyreSmoke").FindChild("stones").gameObject);
		}
	}

	// Update is called once per frame
	void FixedUpdate () {
		Vector3 predictedUp = Quaternion.AngleAxis(
			body.angularVelocity.magnitude * Mathf.Rad2Deg * stability / speed,
			body.angularVelocity
		) * transform.up;

		Vector3 torqueVector = Vector3.Cross(predictedUp, Vector3.up);
		if (speed>0) {
			body.AddTorque(torqueVector * speed * speed);
		}
	}

	// Debug GUI. Disable when not needed.
	void OnGUI ()
	{
//		GUI.Label(new Rect(0,0,100,200), hp.ToString());
//		GUI.Label(new Rect(0,20,100,200), anglefactor.ToString());
		//		GUI.Label (new Rect(0,60,100,200),"km/h: "+rigidbody.velocity.magnitude * 3.6f);
//		tractionControl = GUI.Toggle(new Rect(0,80,300,20), tractionControl, "Traction Control (bypassed by shift key)");
	}

	
}
