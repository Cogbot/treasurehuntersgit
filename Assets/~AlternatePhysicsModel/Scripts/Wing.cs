using UnityEngine;
using System.Collections;

// This class is used to simulate aerodynamic wings or ground effects.
// Add it to a node to have lift or downforce applied at that point.
public class Wing : MonoBehaviour {

	// Cached rigidbody
	public Rigidbody body;
	
	// lift coefficient (use negative values for downforce).
	public float liftCoefficient;
	public float dragCoefficient;

	// Get rigidbody.
	void Start () {
		Transform trs = transform;
		while (trs != null && trs.rigidbody == null)
			trs = trs.parent;
		if (trs != null)
			body = trs.rigidbody;
	}
	
	// Update is called once per frame
	void Update () {
		if (body != null && body.velocity.y<0)
		{
			float lift = body.velocity.sqrMagnitude;
			body.AddForceAtPosition(liftCoefficient * Vector3.up, transform.position);
			//body.AddForceAtPosition(lift * dragCoefficient * transform.forward, transform.position);
//			body.AddRelativeTorque(new Vector3(body.rotation.z*body.rotation.z*body.rotation.z*1000,0,-body.rotation.x*body.rotation.x*body.rotation.x*1000));
		}
	}
}
