using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	public GameObject gemCollectedR;
	public GameObject gemCollectedG;
	public GameObject gemCollectedB;
	public GameObject gemCollectedY;

	public float red, green, blue, yellow;
	public TerrainTools tt;

	//cached
	private GameObject gemCollected;

	public void addScore(GemData.GemCol type, float amount)
	{
		switch (type) {
		case GemData.GemCol.red:
			red += amount;
			break;
		case GemData.GemCol.green:
			blue += amount;
			break;
		case GemData.GemCol.blue:
			green += amount;
			break;
		default:
			yellow += amount;
			break;
		}
	}

	// Collect the gem
	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag.Equals("Gem")) {

			// Find the colour of the sparkles
			switch(other.GetComponent<GemData>().col) {
			case GemData.GemCol.red: gemCollected = gemCollectedR;
				break;
			case GemData.GemCol.green: gemCollected = gemCollectedG;
				break;
			case GemData.GemCol.blue: gemCollected = gemCollectedB;
				break;
			default: gemCollected = gemCollectedY;
				break;
			}
			addScore(other.GetComponent<GemData>().col,1);

			// Create sparkles
			GameObject instance = (GameObject) Instantiate(gemCollected, other.transform.position, Quaternion.identity);
			Random.seed = (int)System.DateTime.Now.Ticks;
			instance.GetComponent<GemCollected>().spinSpeed = Random.Range(-1000,1000)/1000f;
			Destroy(instance.gameObject, 7);

			// Prevent the gem being collected again
			Destroy(other.GetComponent<SphereCollider>());
			other.GetComponent<GemData>().alreadyCollected = true;
			other.GetComponent<Die>().dead = true;
			if (other.transform.childCount>0) {
				Destroy(other.transform.GetChild(0).gameObject);
			}
		}
	}

	void Awake() {tt = GameObject.Find ("Terrain").GetComponent<TerrainTools> ();}

	void Update()
	{
		tt.steepness (transform.position.x, transform.position.z);
	}
}
