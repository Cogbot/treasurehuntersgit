﻿using UnityEngine;
using System.Collections;

public class JoinRoom : MonoBehaviour {
	public string name;
	
	// Update is called once per frame
	public void joinRoom () {
		PhotonNetwork.JoinRoom(name);
	}
}
