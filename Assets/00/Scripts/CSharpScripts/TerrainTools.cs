using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Terrain))]
public class TerrainTools : MonoBehaviour {
	private TerrainData td;
	private Vector3 size;

	void Start() {
		td = transform.GetComponent<Terrain>().terrainData;
		size = td.size;
	}

	public float height(float x, float z)
	{
		return td.GetInterpolatedHeight (x/size.x,z/size.z);
	}

	public Vector3 getSize()
	{
		return td.size;
	}

	public float steepness(float x, float z) {
		return Vector3.Dot (td.GetInterpolatedNormal (x / size.x, z / size.z), Vector3.up);
	}
}
