using UnityEngine;
using System.Collections;

public class ControlPlayer : Player {
	public Vehicle v;
	public float syncWait = 0;
	
	void FixedUpdate() {
		if (v!=null) {
			v.setH(Input.GetAxis ("Horizontal"));
			v.setV(Input.GetAxis("Vertical"));
			syncWait -= Time.fixedDeltaTime;
			if (syncWait<0) {
				v.sync(v.transform.position, v.transform.rotation);
				syncWait = 3;
				Debug.Log("sync");
			}
		}
	}
	
	void OnGUI()
	{
		string text = "Red: " + red + "\nGreen: " + green + "\nBlue: " + blue + "\nYellow: " + yellow;
		GUIStyle s = new GUIStyle ();
		s.fontSize = 40;
		GUI.Label (new Rect (10f, 10f, 100f, 70f), text, s);
  }
}
