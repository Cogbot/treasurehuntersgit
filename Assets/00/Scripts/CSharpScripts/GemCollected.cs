using UnityEngine;
using System.Collections;

public class GemCollected : MonoBehaviour {
	public GameObject gems;
	public GemController gemController;
	public float spinSpeed;

	public AudioClip[] clips;

	//cached
	public float time;
	
	// Use this for initialization
	void Awake () {
		gems = GameObject.Find ("GameScripts");
		gemController = gems.transform.GetComponent <GemController>();
		GetComponent<AudioSource>().clip = clips[Random.Range(0, (clips.Length)-1)];
		GetComponent<AudioSource>().Play();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		time = gemController.timer;
		transform.localRotation = Quaternion.Euler(270, time*50*spinSpeed, 0);
	}
}
