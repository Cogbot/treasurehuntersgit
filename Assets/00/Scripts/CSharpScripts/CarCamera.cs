using UnityEngine;
using System.Collections;

public class CarCamera : MonoBehaviour
{
	public Transform target = null;
	public float height = .5f;
	public float distance = .2f;
	public LayerMask layers;
	public float xRot = 15;

	private RaycastHit hit1 = new RaycastHit();
	private RaycastHit hit2 = new RaycastHit();
	private float lerp1 = 0;
	private float lerp2 = 0;

	private Vector3 up = Vector3.up;

	private Vector3 pos;
	private Vector3 lastpos;
	private Vector3 lastlastpos;

	void LateUpdate()
	{
		// postition camera in default position
		transform.position = target.position;
		Vector3 rot = target.rotation.eulerAngles;
		rot.x += xRot;
		transform.rotation = Quaternion.Euler(rot);
		transform.position -= transform.forward*distance;
		transform.position += up * height;

		// move upwards if intersect with ground
		RaycastHit hit;
		if (Physics.Raycast (transform.position + up * 100, -up, out hit, 300)) {
			if (hit.distance<100+height) transform.position = hit.point + up*height;
		}

		if (lastpos.Equals(null)) {
			lastpos = transform.position;
			lastlastpos = transform.position;
		}

		pos = transform.position;

		transform.position = lastpos;

		lastlastpos = lastpos;
		lastpos = pos;
	}
}
