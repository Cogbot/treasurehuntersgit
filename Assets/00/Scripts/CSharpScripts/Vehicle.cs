using UnityEngine;
using System.Collections;

//[RequireComponent (typeof (PhotonView))]
public class Vehicle : Photon.MonoBehaviour {
	public float h, v;
	public GameObject obj;
//	public NetworkManager nm;

	public virtual void Awake() {
//		nm = GameObject.Find ("GameScripts").GetComponent<NetworkManager> ();
//		Debug.Log(nm.connected);
	}

	public void sync(Vector3 pos, Quaternion quat) {
		if (PhotonNetwork.inRoom) {
			photonView.RPC("rpcSync", PhotonTargets.All, pos, quat);
		} else {
			obj.transform.position = pos;
			obj.transform.rotation = quat;
		}
	}

	[RPC]
	public void rpcSync(Vector3 pos, Quaternion quat) {
		obj.transform.position = pos;
		obj.transform.rotation = quat;
	}

	public void setH(float newH) {
		if (PhotonNetwork.inRoom) {
			photonView.RPC("rpcSetH", PhotonTargets.All, newH);
		} else {
			h = newH;
		}
	}

	public float getH() { return h; }
	[RPC]
	public void rpcSetH(float newH) { h = newH;}
	
	public void setV(float newV) {
		if (PhotonNetwork.inRoom) {
			photonView.RPC("rpcSetV", PhotonTargets.All, newV);
		} else {
			v = newV;
    	}
  	}
	public float getV() { return v; }
	[RPC]
	public void rpcSetV(float newV) { v = newV;}
}
