using UnityEngine;
using System.Collections;

public class NetworkManager : Photon.MonoBehaviour
{
	public bool connected;

	private float btnX, btnY, btnW, btnH;
	private string gameName = "cogmob_island_frenzy";
	private bool refreshing = false;

	private HostData[] hostData;

	void Awake() {
		PhotonNetwork.ConnectUsingSettings ("0.1");
		PhotonNetwork.JoinLobby ();
	}

	void startServer()
	{
		PhotonNetwork.CreateRoom(null);
//		MasterServer.RegisterHost (gameName, "game01", "comment");
		startedHost();
	}

	void refreshHosts()
	{
		MasterServer.RequestHostList (gameName);
		refreshing = true;
	}
		
	void Update ()
	{
		if (!PhotonNetwork.isNonMasterClientInRoom && !PhotonNetwork.isMasterClient) {
			connected = false;
		} else {
			if (!connected) {
				connected = true;
			}
		}

		if (refreshing)
		{
			if (MasterServer.PollHostList().Length > 0)
			{
				refreshing = false;
				hostData = MasterServer.PollHostList();
			}
		}

		btnX = 10;
		btnY = 10;
		btnW = Screen.width*0.2f;
		btnH = Screen.width*0.2f;
	}

	// Messages
	
	void OnMasterServerEvent(MasterServerEvent mse)
	{
		if (mse.Equals(MasterServerEvent.RegistrationSucceeded))
		{
			Debug.Log("Registration Succeeded");
		}
	}

	void OnCreatedRoom() {
		Debug.Log ("Registered Initialized");
	}

	// GUI
	void OnGUI () {
		if (!connected) {
			// Make a background box
			GUI.Box(new Rect(10,10,100,90), "Loader Menu");
			
			// Make the first button. If it is pressed, Application.Loadlevel (1) will be executed
			if(GUI.Button(new Rect(20,40,80,20), "Start Server")) {
				startServer();
			}

			// Make the second button.
			if(GUI.Button(new Rect(20,70,80,20), "Refresh Hosts")) {
				refreshHosts();
			}

			if (hostData != null) {
				for (int i=0; i<hostData.Length; i++) {
					if (GUI.Button (new Rect(btnX*1.5f + btnW, btnY*1.2f + (btnH * i), btnW*3, btnH*0.5f), hostData[i].gameName)) {
						PhotonNetwork.JoinRoom(hostData[i].gameName);
						startedClient();
					}
				}
			}
		}
	}

	// Specific code for Island Frenzy
	public GemController gc;
	public VehicleSwitcher vs;
	public void startedHost() {
			Debug.Log ("calling to gc");
			gc.createSpawnPoints ();
	}

	public void startedClient() {
	}
}