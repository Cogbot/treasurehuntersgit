using UnityEngine;
using System.Collections;

[RequireComponent(typeof (Rigidbody))]
public class speedBoost : MonoBehaviour {
	public float amount = 10000;
	public Vehicle v;

	void LateUpdate ()
	{
		if (v.getV()>0) {
			rigidbody.AddForceAtPosition (rigidbody.transform.forward*v.getV ()*amount, rigidbody.position);
		} else {
			rigidbody.AddForceAtPosition (rigidbody.transform.forward*v.getV ()*amount/4, rigidbody.position);
		}
	}
}