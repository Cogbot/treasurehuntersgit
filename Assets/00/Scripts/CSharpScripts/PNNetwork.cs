﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PNNetwork : Photon.MonoBehaviour {

	public GameObject joinButton;
	public GameObject joinPanel;
	public Canvas joinCanvas;

	private string roomName = "myRoom";
	private bool connectFailed = false;

	// cached
	private int lastRoomCount = 10;
	private RoomInfo[] roomList;

	// Use this for initialization
	void Awake () {
		// this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
		PhotonNetwork.automaticallySyncScene = true;
		
		// the following line checks if this client was just created (and not yet online). if so, we connect
		if (PhotonNetwork.connectionStateDetailed == PeerState.PeerCreated)
		{
			// Connect to the photon master-server. We use the settings saved in PhotonServerSettings (a .asset file in this project)
			PhotonNetwork.ConnectUsingSettings("0.9");
		}
		
		// generate a name for this player, if none is assigned yet
		if (string.IsNullOrEmpty(PhotonNetwork.playerName))
		{
			PhotonNetwork.playerName = "Guest" + Random.Range(1, 9999);
		}

		// PhotonNetwork.logLevel = NetworkLogLevel.Full;
	}

	void Update() {
		// if you wanted more debug out, turn this on:
		// PhotonNetwork.logLevel = NetworkLogLevel.Full;
		roomList = PhotonNetwork.GetRoomList ();
		if (joinCanvas.enabled) {
			if (roomList.Length != lastRoomCount) {
				Debug.Log ("room count changed (was "+lastRoomCount+" now "+roomList.Length+")");
				// Remove current buttons
				foreach(Transform child in joinPanel.transform) {
					if (!child.gameObject.name.Equals("Prototype"))
					    Destroy(child.gameObject);
				}

				// Add new buttons
				int i=0;
				foreach (RoomInfo roomInfo in PhotonNetwork.GetRoomList())
				{
					GameObject newButton = (GameObject) Instantiate(joinButton);
					newButton.transform.parent = joinPanel.transform;
					RectTransform rt = newButton.transform.GetComponent<RectTransform>();
					rt.sizeDelta = new Vector2(0, 50);
					rt.localPosition = new Vector3(0,0,0);
					rt.anchoredPosition = new Vector2(0,0);
					rt.Translate(new Vector2(0,-50-75*i));
					newButton.transform.GetChild(0).gameObject.GetComponent<Text>().text = roomInfo.name;
					newButton.GetComponent<JoinRoom>().name = roomInfo.name;

//					GUILayout.BeginHorizontal();
//					GUILayout.Label(roomInfo.name + " " + roomInfo.playerCount + "/" + roomInfo.maxPlayers);
//					if (GUILayout.Button("Join"))
//					{
//						PhotonNetwork.JoinRoom(roomInfo.name);
//					}
//					
//					GUILayout.EndHorizontal();
					i++;
				}
			}
			lastRoomCount = roomList.Length;
		}
	}

	public void CreateRoom() {
		PhotonNetwork.CreateRoom(this.roomName, new RoomOptions() { maxPlayers = 10 }, null);
	}











	
	// We have two options here: we either joined(by title, list or random) or created a room.
	public void OnJoinedRoom()
	{
		Debug.Log("OnJoinedRoom");
	}
	
	
	public void OnPhotonCreateRoomFailed()
	{
//		this.ErrorDialog = "Error: Can't create room (room name maybe already used).";
		Debug.Log("OnPhotonCreateRoomFailed got called. This can happen if the room exists (even if not visible). Try another room name.");
	}
	
	public void OnPhotonJoinRoomFailed()
	{
//		this.ErrorDialog = "Error: Can't join room (full or unknown room name).";
		Debug.Log("OnPhotonJoinRoomFailed got called. This can happen if the room is not existing or full or closed.");
	}
	public void OnPhotonRandomJoinFailed()
	{
//		this.ErrorDialog = "Error: Can't join random room (none found).";
		Debug.Log("OnPhotonRandomJoinFailed got called. Happens if no room is available (or all full or invisible or closed). JoinrRandom filter-options can limit available rooms.");
	}
	
	public VehicleSwitcher vs;
	public GemController gc;
	public void OnCreatedRoom()
	{
		Debug.Log("OnCreatedRoom");
		vs.respawn ();
		gc.createSpawnPoints ();
//		PhotonNetwork.LoadLevel(SceneNameGame);
	}
	
	public void OnDisconnectedFromPhoton()
	{
		Debug.Log("Disconnected from Photon.");
	}
	
	public void OnFailedToConnectToPhoton(object parameters)
	{
		this.connectFailed = true;
		Debug.Log("OnFailedToConnectToPhoton. StatusCode: " + parameters + " ServerAddress: " + PhotonNetwork.networkingPeer.ServerAddress);
	}

	public void OnApplicationQuit()
	{
		PhotonNetwork.Disconnect ();
	}
}
