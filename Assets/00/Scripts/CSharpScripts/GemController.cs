using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;

public class GemController : Photon.MonoBehaviour {
	private GameObject gemSpawns;
	public GameObject delayGem;

//	public GameObject gemPrimative;
	public GameObject gemSpawn;
	public float timer = 0;

	public VehicleSwitcher switcher;

	public TerrainTools tt;

	public static int i=0;

	//cached
	private GemData.GemCol col;
	private AnimationCurve xCurve, yCurve, zCurve;
	private float timeBetweenKeys;
	private float totalCurveTime;
	private Vector3 lastPosition = Vector3.zero;
	private float gemTime;
	private int keyCount;
	private List<Vector3> spawnVectors;
	private float xVec, yVec, zVec, startAngle, lastYVec;

	// Use this for initialization
	void Start ()
	{
		Random.seed = (int)System.DateTime.Now.Ticks;
		//createSpawnPoints ();
		tt = GameObject.Find ("Terrain").transform.GetComponent<TerrainTools> ();
		gemSpawns = GameObject.Find ("GemSpawns");
	}
	
	public void createSpawnPoints ()
	{
		Debug.Log ("creating spawn points");
		int spawnCount = 50;
		for (int i=1; i<spawnCount; i++)
		{
			xVec = tt.getSize().x*Random.Range(1000,9000)/10000f;
			zVec = tt.getSize().z*Random.Range(1000,9000)/10000f;
			yVec = tt.height(xVec,zVec);

			bool spawnOk = true;
			// Check the spawn will not be underwater
			if (yVec<GameObject.Find("Water").transform.position.y) spawnOk = false;
			// Check the spawn is not in a cliff
			if (Mathf.Abs (tt.height(xVec+3,zVec)-yVec)>1) spawnOk = false;
			if (Mathf.Abs (tt.height(xVec-3,zVec)-yVec)>1) spawnOk = false;
			if (Mathf.Abs (tt.height(xVec,zVec+3)-yVec)>1) spawnOk = false;
			if (Mathf.Abs (tt.height(xVec,zVec-3)-yVec)>1) spawnOk = false;

			if (spawnOk) {
				Debug.Log(gemSpawn.name);
				if (PhotonNetwork.inRoom) {
					PhotonNetwork.Instantiate (gemSpawn.name,
				                                             new Vector3(xVec, yVec, zVec),
				                                             Quaternion.identity,0);
				} else {
					Instantiate (gemSpawn,
					                           new Vector3(xVec, yVec, zVec),
					                           Quaternion.identity);
				}
			}
		}
  }
  
  // Update is called once per frame
	void FixedUpdate ()
	{
		timer+=Time.fixedDeltaTime;
	}
}
